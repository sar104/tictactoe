//
//  ViewController.swift
//  TicTacToe
//
//  Created by Apple on 4/24/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var lblWon: UILabel!
    
    var dicBtnVAls: [Int: String] = [:]
    var userTurn: Bool = true
    
    var win1: [Int] = [1,2,3]
    var win2: [Int] = [4,5,6]
    var win3: [Int] = [7,8,9]
    var win4: [Int] = [1,4,7]
    var win5: [Int] = [2,5,8]
    var win6: [Int] = [3,6,9]
    var win7: [Int] = [1,5,9]
    var win8: [Int] = [3,5,7]
    
    var winArr: [[Int]] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        winArr = [win1, win2, win3, win4, win5, win6, win7, win8]
        reset()
        
    }
    func createDic() {
        
        dicBtnVAls.removeAll()
        for i in 1...9 {
            //print("\(view.viewWithTag(i))")
            dicBtnVAls[i] = ""
            let btn = view.viewWithTag(i) as! UIButton
            btn.setTitle("", for: .normal)
        }
        print(dicBtnVAls)
    }
    
    @IBAction func reset() {
        
        userTurn = true
        lblWon.text = "Your turn"
        createDic()
    }
    
    func continuePlay()-> Bool {
        let empty = dicBtnVAls.first { (k,v) -> Bool in
            return v.isEmpty
        }
        if empty != nil {
            return true
        }
        lblWon.text = "Game Over!"
        return false
    }
    
    @IBAction func clicked(_ sender: UIButton){
        
        if continuePlay() && userTurn {
            userTurn = false
            //print("\(sender.tag)")
            /*
            if let title = sender.title(for: .normal), !title.isEmpty {
                //print("\(title)")
            } else {
                sender.setTitle("X", for: .normal)
                dicBtnVAls[sender.tag] = "A"
            }*/
            setVal(sender: sender, title: "X", value: "A")
            //print(dicBtnVAls)
            if !checkWin() {
                if continuePlay() {
                    lblWon.text = "O turn"
                    let delay = DispatchTime.now() + 2
                    DispatchQueue.main.asyncAfter(deadline: delay) {
                        self.smartPlay()
                    }
                }
            }
        }
        
    }
    
    @IBAction func sysClicked(_ sender: UIButton){
        
        //print("\(sender.tag)")
        /*
        if let title = sender.title(for: .normal), !title.isEmpty {
            //print("\(title)")
        } else {
            sender.setTitle("O", for: .normal)
            dicBtnVAls[sender.tag] = "B"
        }*/
        setVal(sender: sender, title: "O", value: "B")
        //print(dicBtnVAls)
        if !checkWin() {
            userTurn = true
            lblWon.text = "X turn"
        }
    }
    
    func setVal(sender: UIButton, title: String, value: String){
        
        if let strTitle = sender.title(for: .normal), !strTitle.isEmpty {
            print("\(strTitle)")
        } else {
            sender.setTitle(title, for: .normal)
            dicBtnVAls[sender.tag] = value
        }
        
        if value == "A" {
            sender.setTitleColor(.red, for: .normal)
        }
    }
    
    func play() {
        //sysClicked(view.viewWithTag(5) as! UIButton)
        let btn = dicBtnVAls.first { (k, v) -> Bool in
            return v.isEmpty
        }
        if btn != nil {
            sysClicked(view.viewWithTag(btn!.key) as! UIButton)
        }
    }
    
    func smartPlay() {
        
        var found = false
        let winA = dicBtnVAls.filter { (k,v) -> Bool in
            return v == "A"
            }.keys.sorted()
        
        
        let setA = Set(winA)
        
        let winB = dicBtnVAls.filter { (k,v) -> Bool in
            return v == "B"
            }.keys.sorted()
        
        let setB = Set(winB)
        var commonArr: [Int] = []
        for i in 0..<winArr.count {
            
            let findset = Set(winArr[i])
            commonArr = winA.filter { findset.contains($0)}
            if commonArr.count > 1 {
                
                let nextMove = findset.first { (v) -> Bool in
                    return !setA.contains(v) && !setB.contains(v)
                }
                if nextMove != nil {
                    found = true
                    sysClicked(view.viewWithTag(nextMove!) as! UIButton)
                    break
                }
            }
        }
       
        if !found {
            
            for i in 0..<winArr.count {
                
                let findset = Set(winArr[i])
                
                let equals = setA.isSubset(of: findset)
                
                if equals {
                    
                    let nextMove = findset.first { (v) -> Bool in
                        return !setA.contains(v) && !setB.contains(v)
                    }
                    if nextMove != nil {
                        found = true
                        sysClicked(view.viewWithTag(nextMove!) as! UIButton)
                        break
                    }
                    break
                }
            }
        }
        if !found {
            for i in 0..<winArr.count {
                
                let findset = Set(winArr[i])
                commonArr = winB.filter { findset.contains($0)}
                if commonArr.count > 0 {
                    
                    let nextMove = findset.first { (v) -> Bool in
                        return !setA.contains(v) && !setB.contains(v)
                    }
                    if nextMove != nil {
                        found = true
                        sysClicked(view.viewWithTag(nextMove!) as! UIButton)
                        break
                    }
                }
            }
        }
    }
    
    func checkWin() -> Bool {
        
        let winA = dicBtnVAls.filter { (k,v) -> Bool in
            return v == "A"
            }.keys.sorted()
        
        let setA = Set(winA)
        
        for i in 0..<winArr.count {
            
            let findset = Set(winArr[i])
            let equals = findset.isSubset(of: setA)
            if equals {
                print("You Won")
                lblWon.text = "X Won"
                return true
            }
        }
        
        let winB = dicBtnVAls.filter { (k,v) -> Bool in
            return v == "B"
            }.keys.sorted()
        
        let setB = Set(winB)
        
        for i in 0..<winArr.count {
            
            let findset = Set(winArr[i])
            let equals = findset.isSubset(of: setB)
            if equals {
                print("I Won")
                lblWon.text = "O Won"
                return true
            }
        }
        return false
        
    }
}

